// Module
let fetchChannelDataApiMethod;

const CACHE = {};


// {{{ Helpers
function getCacheLocation(experimentId, channelId) {
  if (!CACHE.hasOwnProperty(experimentId)) {
    CACHE[experimentId] = {};
  }
  if (!CACHE[experimentId].hasOwnProperty(channelId)) {
    CACHE[experimentId][channelId] = {
      sections: [],
    };
  }

  return CACHE[experimentId][channelId];
}


function getSectionEnd(section) {
  return section.offset + section.values.length;
}


function getChannelTotalItemCount({experimentId, channelId}) {
  try {
    return CACHE[experimentId][channelId].totalItemCount || null;
  } catch (e) {
    return null;
  }
}
// Helpers }}}


function addValuesToCache({experimentId, offset, channelData}) {
  const {
    channelId,
    totalItemCount,
    values,
  } = channelData;

  const cache = getCacheLocation(experimentId, channelId);
  cache.totalItemCount = totalItemCount;

  let newSection = {
    offset: (offset === null) ? (totalItemCount - values.length) : offset,
    values: [...values],
  };
  const newSectionEnd = getSectionEnd(newSection);

  if (cache.sections.length === 0) {
    cache.sections.push(newSection);
    return;
  }

  cache.sections = cache.sections.reduce((sections, section) => {
    const sectionEnd = getSectionEnd(section);

    if (!newSection || newSectionEnd < section.offset || sectionEnd < newSection.offset) {
      // disjoint
      // #1  \\\ ///
      // #13 /// \\\
      sections.push(section);
    } else if (newSection.offset <= section.offset && newSectionEnd < sectionEnd) {
      // new section overlaps an old section from the left
      // #2 \\\///
      // #3 \\XX//
      // #7 XXX//
      const tailValues = section.values.slice(newSectionEnd - section.offset);
      newSection.values.push(...tailValues);
    } else if (section.offset < newSection.offset) {
      if (sectionEnd <= newSectionEnd) {
        // new section overlaps an old section from the right
        // #9  //XXX
        // #11 //XX\\
        // #12 ///\\\
        const headValues = section.values.slice(0, newSection.offset - section.offset);
        newSection.values.unshift(...headValues);
        newSection.offset = section.offset;
      } else {
        // an old section contains the new section
        // #8 //XXX//
        newSection = null;
        sections.push(section);
      }
    }
    // For the following cases do nothing:
    // #4  \\XXX
    // #5  \\XXX\\
    // #6  XXX
    // #10 XXX\\

    return sections;
  }, []);

  if (newSection) {
    cache.sections.push(newSection);
    cache.sections.sort((s1, s2) => s1.offset - s2.offset);
  }
}


function cacheExist(experimentId) {
  return CACHE.hasOwnProperty(experimentId);
}


function fetchChannelData({experimentId, channelId, offset, limit}) {
  return fetchChannelDataApiMethod({channelId, offset, limit})
    .then((channelData) => {
      addValuesToCache({
        experimentId,
        offset,
        channelData,
      });

      return channelData;
    });
}


function getChannelData({experimentId, channelId, offset = null, limit} = {}) {
  if (offset === null) {
    return fetchChannelData({experimentId, channelId, offset, limit});
  }

  const values = getValuesFromCache({experimentId, channelId, offset, limit});

  if (!values) {
    return fetchChannelData({experimentId, channelId, offset, limit});
  }

  const totalItemCount = getChannelTotalItemCount({experimentId, channelId});

  return Promise
    .resolve({
      channelId,
      totalItemCount,
      values,
    });
}


function getValuesFromCache({experimentId, channelId, offset, limit}) {
  const channel = CACHE[experimentId] && CACHE[experimentId][channelId] || null;

  if (!channel) { // no experiment / channel
    return null;
  }

  // find section
  const section = channel.sections.find((section) => {
    return section.offset <= offset && offset + limit <= getSectionEnd(section);
  });
  if (!section) {
    return null;
  }

  const subSectionBegin = offset - section.offset;
  return section.values.slice(subSectionBegin, subSectionBegin + limit);
}


function injectApiMethod(apiMethod) {
  fetchChannelDataApiMethod = apiMethod;
}


function invalidateCache() {
  Object.keys(CACHE).forEach((experimentId) => {
    delete CACHE[experimentId];
  });
}


export default {
  CACHE,
  addValuesToCache,
  cacheExist,
  fetchChannelData,
  getChannelData,
  getValuesFromCache,
  injectApiMethod,
  invalidateCache,
};
