import channelsDatasource from './channelsDatasource';


describe('channelsDatasource', () => {


  describe('at the very beginning', () => {
    it('has got empty cache', () => {
      // Assert
      expect(channelsDatasource.CACHE).toEqual({});
    });
  });


  describe('addValuesToCache', () => {
    beforeEach(() => {
      // Cleanup
      delete channelsDatasource.CACHE.myExperiment;
    });


    it('adds first values to cache', () => {
      // Act
      channelsDatasource.addValuesToCache({
        experimentId: 'myExperiment',
        offset: 5,
        channelData: {
          channelId: 'myChannel',
          totalItemCount: 10,
          values: [5, 6, 7, 8, 9],
        },
      });

      // Assert
      expect(channelsDatasource.CACHE).toEqual({
        myExperiment: {
          myChannel: {
            totalItemCount: 10,
            sections: [
              {
                offset: 5,
                values: [5, 6, 7, 8, 9],
              },
            ],
          },
        },
      });
    });


    it('calculates proper offset when adding N last values', () => {
      // Act
      channelsDatasource.addValuesToCache({
        experimentId: 'myExperiment',
        offset: null,
        channelData: {
          channelId: 'myChannel',
          totalItemCount: 10,
          values: [5, 6, 7, 8, 9],
        },
      });

      // Assert
      expect(channelsDatasource.CACHE).toEqual({
        myExperiment: {
          myChannel: {
            totalItemCount: 10,
            sections: [
              {
                offset: 5,
                values: [5, 6, 7, 8, 9],
              },
            ],
          },
        },
      });
    });


    describe('adds another values to cache (new: \\\\\\, existing: ///)', () => {
      beforeEach(() => {
        // The CACHE is empty because of addValuesToCache.beforeEach()
        channelsDatasource.addValuesToCache({
          experimentId: 'myExperiment',
          offset: 5,
          channelData: {
            channelId: 'myChannel',
            totalItemCount: 100,
            values: [5, 6, 7, 8, 9],
          },
        });
      });


      it('#1 \\\\\\ ///', () => {
        // Act
        channelsDatasource.addValuesToCache({
          experimentId: 'myExperiment',
          offset: 0,
          channelData: {
            channelId: 'myChannel',
            totalItemCount: 100,
            values: [0, 1, 2, 3],
          },
        });

        // Assert
        const sections = channelsDatasource.CACHE.myExperiment.myChannel.sections;
        expect(sections.length).toBe(2);
        expect(sections[0].offset).toBe(0);
        expect(sections[0].values).toEqual([0, 1, 2, 3]);
        expect(sections[1].offset).toBe(5);
        expect(sections[1].values).toEqual([5, 6, 7, 8, 9]);
      });


      it('#2 \\\\\\///', () => {
        // Act
        channelsDatasource.addValuesToCache({
          experimentId: 'myExperiment',
          offset: 0,
          channelData: {
            channelId: 'myChannel',
            totalItemCount: 100,
            values: [0, 1, 2, 3, 4],
          },
        });

        // Assert
        const sections = channelsDatasource.CACHE.myExperiment.myChannel.sections;
        expect(sections.length).toBe(1);
        expect(sections[0].offset).toBe(0);
        expect(sections[0].values).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);

        expect(sections).toEqual([
          {
            offset: 0,
            values: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
          },
        ]);
      });


      it('#3 \\\\XX//', () => {
        // Act
        channelsDatasource.addValuesToCache({
          experimentId: 'myExperiment',
          offset: 0,
          channelData: {
            channelId: 'myChannel',
            totalItemCount: 100,
            values: [0, 1, 2, 3, 4, 5, 6],
          },
        });

        // Assert
        const sections = channelsDatasource.CACHE.myExperiment.myChannel.sections;
        expect(sections.length).toBe(1);
        expect(sections[0].offset).toBe(0);
        expect(sections[0].values).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
      });


      it('#4 \\\\XXX', () => {
        // Act
        channelsDatasource.addValuesToCache({
          experimentId: 'myExperiment',
          offset: 0,
          channelData: {
            channelId: 'myChannel',
            totalItemCount: 100,
            values: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
          },
        });

        // Assert
        const sections = channelsDatasource.CACHE.myExperiment.myChannel.sections;
        expect(sections.length).toBe(1);
        expect(sections[0].offset).toBe(0);
        expect(sections[0].values).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
      });


      it('#5 \\\\XXX\\\\', () => {
        // Act
        channelsDatasource.addValuesToCache({
          experimentId: 'myExperiment',
          offset: 4,
          channelData: {
            channelId: 'myChannel',
            totalItemCount: 100,
            values: [4, 5, 6, 7, 8, 9, 10],
          },
        });

        // Assert
        const sections = channelsDatasource.CACHE.myExperiment.myChannel.sections;
        expect(sections.length).toBe(1);
        expect(sections[0].offset).toBe(4);
        expect(sections[0].values).toEqual([4, 5, 6, 7, 8, 9, 10]);
      });


      it('#6 XXX', () => {
        // Act
        channelsDatasource.addValuesToCache({
          experimentId: 'myExperiment',
          offset: 5,
          channelData: {
            channelId: 'myChannel',
            totalItemCount: 100,
            values: [5, 6, 7, 8, 9],
          },
        });

        // Assert
        const sections = channelsDatasource.CACHE.myExperiment.myChannel.sections;
        expect(sections.length).toBe(1);
        expect(sections[0].offset).toBe(5);
        expect(sections[0].values).toEqual([5, 6, 7, 8, 9]);
      });


      it('#7 XXX//', () => {
        // Act
        channelsDatasource.addValuesToCache({
          experimentId: 'myExperiment',
          offset: 5,
          channelData: {
            channelId: 'myChannel',
            totalItemCount: 100,
            values: [5, 6, 7, 8],
          },
        });

        // Assert
        const sections = channelsDatasource.CACHE.myExperiment.myChannel.sections;
        expect(sections.length).toBe(1);
        expect(sections[0].offset).toBe(5);
        expect(sections[0].values).toEqual([5, 6, 7, 8, 9]);
      });


      it('#8 //XXX//', () => {
        // Act
        channelsDatasource.addValuesToCache({
          experimentId: 'myExperiment',
          offset: 6,
          channelData: {
            channelId: 'myChannel',
            totalItemCount: 100,
            values: [6, 7, 8],
          },
        });

        // Assert
        const sections = channelsDatasource.CACHE.myExperiment.myChannel.sections;
        expect(sections.length).toBe(1);
        expect(sections[0].offset).toBe(5);
        expect(sections[0].values).toEqual([5, 6, 7, 8, 9]);
      });


      it('#9 //XXX', () => {
        // Act
        channelsDatasource.addValuesToCache({
          experimentId: 'myExperiment',
          offset: 6,
          channelData: {
            channelId: 'myChannel',
            totalItemCount: 100,
            values: [6, 7, 8, 9],
          },
        });

        // Assert
        const sections = channelsDatasource.CACHE.myExperiment.myChannel.sections;
        expect(sections.length).toBe(1);
        expect(sections[0].offset).toBe(5);
        expect(sections[0].values).toEqual([5, 6, 7, 8, 9]);
      });


      it('#10 XXX\\\\', () => {
        // Act
        channelsDatasource.addValuesToCache({
          experimentId: 'myExperiment',
          offset: 5,
          channelData: {
            channelId: 'myChannel',
            totalItemCount: 100,
            values: [5, 6, 7, 8, 9, 10],
          },
        });

        // Assert
        const sections = channelsDatasource.CACHE.myExperiment.myChannel.sections;
        expect(sections.length).toBe(1);
        expect(sections[0].offset).toBe(5);
        expect(sections[0].values).toEqual([5, 6, 7, 8, 9, 10]);
      });


      it('#11 //XX\\\\', () => {
        // Act
        channelsDatasource.addValuesToCache({
          experimentId: 'myExperiment',
          offset: 7,
          channelData: {
            channelId: 'myChannel',
            totalItemCount: 100,
            values: [7, 8, 9, 10, 11],
          },
        });

        // Assert
        const sections = channelsDatasource.CACHE.myExperiment.myChannel.sections;
        expect(sections.length).toBe(1);
        expect(sections[0].offset).toBe(5);
        expect(sections[0].values).toEqual([5, 6, 7, 8, 9, 10, 11]);
      });


      it('#12 ///\\\\\\', () => {
        // Act
        channelsDatasource.addValuesToCache({
          experimentId: 'myExperiment',
          offset: 10,
          channelData: {
            channelId: 'myChannel',
            totalItemCount: 100,
            values: [10, 11, 12, 13, 14],
          },
        });

        // Assert
        const sections = channelsDatasource.CACHE.myExperiment.myChannel.sections;
        expect(sections.length).toBe(1);
        expect(sections[0].offset).toBe(5);
        expect(sections[0].values).toEqual([5, 6, 7, 8, 9, 10, 11, 12, 13, 14]);
      });


      it('#13 /// \\\\\\', () => {
        // Act
        channelsDatasource.addValuesToCache({
          experimentId: 'myExperiment',
          offset: 11,
          channelData: {
            channelId: 'myChannel',
            totalItemCount: 100,
            values: [11, 12, 13, 14],
          },
        });

        // Assert
        const sections = channelsDatasource.CACHE.myExperiment.myChannel.sections;
        expect(sections.length).toBe(2);
        expect(sections[0].offset).toBe(5);
        expect(sections[0].values).toEqual([5, 6, 7, 8, 9]);
        expect(sections[1].offset).toBe(11);
        expect(sections[1].values).toEqual([11, 12, 13, 14]);
      });
    });
  });


  describe('getValuesFromCache', () => {
    beforeEach(() => {
      // Cleanup
      delete channelsDatasource.CACHE.myExperiment;
      // Setup
      channelsDatasource.addValuesToCache({
        experimentId: 'myExperiment',
        offset: 5,
        channelData: {
          channelId: 'myChannel',
          totalItemCount: 100,
          values: [5, 6, 7, 8, 9],
        },
      });
    });


    describe('returns values', () => {
      it('if requiring set stored in cache', () => {
        // Act
        const result = channelsDatasource.getValuesFromCache({
          experimentId: 'myExperiment',
          channelId: 'myChannel',
          offset: 5,
          limit: 5,
        });

        // Assert
        expect(result).toEqual([5, 6, 7, 8, 9]);
      });


      it('if requiring subset stored in cache', () => {
        // Act
        const result1 = channelsDatasource.getValuesFromCache({
          experimentId: 'myExperiment',
          channelId: 'myChannel',
          offset: 5,
          limit: 4,
        });
        const result2 = channelsDatasource.getValuesFromCache({
          experimentId: 'myExperiment',
          channelId: 'myChannel',
          offset: 6,
          limit: 4,
        });
        const result3 = channelsDatasource.getValuesFromCache({
          experimentId: 'myExperiment',
          channelId: 'myChannel',
          offset: 6,
          limit: 3,
        });

        // Assert
        expect(result1).toEqual([5, 6, 7, 8]);
        expect(result2).toEqual([6, 7, 8, 9]);
        expect(result3).toEqual([6, 7, 8]);
      });
    });


    describe('returns null', () => {
      it('if there is no data for given channel in experiment', () => {
        // Act
        const result = channelsDatasource.getValuesFromCache({
          experimentId: 'myExperiment',
          channelId: 'noChannel',
        });

        // Assert
        expect(result).toBeNull();
      });


      it('if there is no data for given experiment', () => {
        // Act
        const result = channelsDatasource.getValuesFromCache({
          experimentId: 'noExperiment',
          channelId: 'myChannel',
        });

        // Assert
        expect(result).toBeNull();
      });


      it('if requiring set not stored in the cache', () => {
        // Act
        const result = channelsDatasource.getValuesFromCache({
          experimentId: 'myExperiment',
          channelId: 'myChannel',
          offset: 0,
          limit: 4,
        });

        // Assert
        expect(result).toBeNull();
      });


      it('if requiring set partially stored in cache', () => {
        // Act
        const result1 = channelsDatasource.getValuesFromCache({
          experimentId: 'myExperiment',
          channelId: 'myChannel',
          offset: 4,
          limit: 5,
        });
        const result2 = channelsDatasource.getValuesFromCache({
          experimentId: 'myExperiment',
          channelId: 'myChannel',
          offset: 6,
          limit: 5,
        });
        const result3 = channelsDatasource.getValuesFromCache({
          experimentId: 'myExperiment',
          channelId: 'myChannel',
          offset: 4,
          limit: 7,
        });

        // Assert
        expect(result1).toBeNull();
        expect(result2).toBeNull();
        expect(result3).toBeNull();
      });
    });

  });
});
