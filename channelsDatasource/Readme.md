Use case
========

Many experiments can have many channels.
Every channel can have huge number of records (500K - 2M is a common case)
Channel data can be presented by UI component using infinite scroll like approach.

Module supports UI component returning required subset of data.
Module can fetch data from endpoind if required.
Fetched data are cached.

Code is ~3 years old, writen using TDD approach.
