Set of components to layout and align the content.

Components are UI agnostic.

Every component consists of code (TypeScript), Less (if needed) and documentation (Storybook)
