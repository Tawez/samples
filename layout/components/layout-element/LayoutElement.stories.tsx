// Libs
import React from 'react';
import {
  text,
  boolean,
} from '@storybook/addon-knobs';


// Layout
import {
  alignSelfKnob,
  layoutUnitKnob,
  overflowKnob,
  spanKnob,
} from 'layout/_doc_helpers/storybook';

import { Layout } from 'layout/components';


// Module
import {
  LayoutElement,
  LayoutElementProps,
} from './LayoutElement';


export default {
  title: 'Layout System/LayoutElement',
  component: LayoutElement,
  excludeStories: /.*Data$/,
};


export const Playground = () => {

  const siblingsSpan = spanKnob({ groupId: 'Siblings'});

  return (
    <Layout.Row spacedChildren="md" height="50px">
      <Layout.Element className="guide--gray-cc" span={siblingsSpan}><em>element</em></Layout.Element>
      <Layout.Element className="guide--gray-cc" span={siblingsSpan}><em>element</em></Layout.Element>
      <Layout.Element
        className="guide--blue"
        children={text('Content', 'Lorem ipsum...', 'Element')}
        alignSelf={alignSelfKnob({ groupId: 'Element' })}
        selfSpacing={layoutUnitKnob({ label: 'selfSpacing', groupId: 'Element'})}
        span={spanKnob({ groupId: 'Element'})}
        height={text('height', '', 'Element')}
        width={text('width', '', 'Element')}
        withGutter={layoutUnitKnob({ label: 'withGutter', groupId: 'Element'})}
        withPadding={layoutUnitKnob({ label: 'withPadding', groupId: 'Element'})}
        overflow={overflowKnob({groupId: 'Element'})}
      />
      <Layout.Element className="guide--gray-cc" span={siblingsSpan}><em>element</em></Layout.Element>
      <Layout.Element className="guide--gray-cc" span={siblingsSpan}><em>element</em></Layout.Element>
    </Layout.Row>
  );
};


export const alignSelf = () => (
  <Layout.Row
    alignItems="center"
    className="guide--blue guide--zebra-children"
    height={100}
  >
    <Layout.Element>Lorem ipsum<br />dolor sit amet</Layout.Element>
    <Layout.Element alignSelf="auto">auto</Layout.Element>
    <Layout.Element alignSelf="start">start</Layout.Element>
    <Layout.Element alignSelf="end">end</Layout.Element>
    <Layout.Element alignSelf="center">center</Layout.Element>
    <Layout.Element alignSelf="baseline">baseline</Layout.Element>
    <Layout.Element alignSelf="stretch">stretch</Layout.Element>
  </Layout.Row>
);

alignSelf.story = {
  parameters: {
    docs: {
      storyDescription: 'Allows the alignment set in the container using `align-items` to be overridden for individual flex item',
    },
  },
};


export const widthAndHeight = () => (
  <Layout.Row height={140} className="guide--blue">
    <Layout.Element width="200px" height={150} span="greedy" className="guide--green">
      200px &times; 150 &amp; span:greedy
    </Layout.Element>
    <Layout.Element width="10vmax" height="15vh" span="greedy" className="guide--yellow">
      10vmax &times; 15vh
    </Layout.Element>
    <Layout.Element width="10em" height="50%" className="guide--orange">
      10em &times; 50%
    </Layout.Element>
    <Layout.Element span="greedy" className="guide--red">
      span:greedy
    </Layout.Element>
  </Layout.Row>
);

widthAndHeight.story = {
  parameters: {
    docs: {
      storyDescription: 'If `height` or `width` is set, `span` will be forced to `"auto"`',
    },
  },
};


export const overflow = () => {
  const elementStyle: LayoutElementProps<{}> = {
    height: '100px',
    className: 'guide--blue',
    withPadding: 'md',
  };

  return (
    <Layout.Column spacedChildren="lg" width="400px">
      <Layout.Element selfSpacing="sm"><strong>visible</strong> - not specified (default)</Layout.Element>
      <Layout.Element overflow="visible" {...elementStyle}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Nuncatsemfeugiatmassaiaculisvehiculaegetquisenim.Etiamsitametbibendumlacus.Infinibusquamnonportahendrerit.
        Integer non sem ipsum.
        Aenean congue nisl eu turpis tincidunt, in scelerisque est tempor.
        Phasellus hendrerit eros ac semper sodales.
      </Layout.Element>

      <Layout.Element selfSpacing="sm"><strong>"auto"</strong></Layout.Element>
      <Layout.Element overflow="auto" {...elementStyle}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at sem
        feugiat massa iaculis vehicula eget quis enim. Etiam sit amet bibendum
        lacus. In finibus quam non porta hendrerit. Integer non sem ipsum.
        Aenean congue nisl eu turpis tincidunt, in scelerisque est tempor.
        Phasellus hendrerit eros ac semper sodales.
      </Layout.Element>

      <Layout.Element selfSpacing="sm"><strong>"hidden"</strong></Layout.Element>
      <Layout.Element overflow="hidden" {...elementStyle}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Nuncatsemfeugiatmassaiaculisvehiculaegetquisenim.Etiamsitametbibendumlacus.Infinibusquamnonportahendrerit.
        Integer non sem ipsum. Aenean congue nisl eu turpis tincidunt, in
        scelerisque est tempor. Phasellus hendrerit eros ac semper sodales.
      </Layout.Element>

      <Layout.Element selfSpacing="sm"><strong>"scroll"</strong></Layout.Element>
      <Layout.Element overflow="scroll" {...elementStyle}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Nuncatsemfeugiatmassaiaculisvehiculaegetquisenim.Etiamsitametbibendumlacus.Infinibusquamnonportahendrerit.
        Integer non sem ipsum. Aenean congue nisl eu turpis tincidunt, in
        scelerisque est tempor. Phasellus hendrerit eros ac semper sodales.
      </Layout.Element>

      <Layout.Element selfSpacing="sm"><strong>{'{'}vertical: 'auto'{'}'}</strong></Layout.Element>
      <Layout.Element overflow={{ vertical: 'auto' }} {...elementStyle}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Nuncatsemfeugiatmassaiaculisvehiculaegetquisenim.Etiamsitametbibendumlacus.Infinibusquamnonportahendrerit.
        Integer non sem ipsum.
        Aenean congue nisl eu turpis tincidunt, in scelerisque est tempor.
        Phasellus hendrerit eros ac semper sodales.
      </Layout.Element>

      <Layout.Element selfSpacing="sm"><strong>{'{'}horizontal: 'auto'{'}'}</strong></Layout.Element>
      <Layout.Element overflow={{ horizontal: 'auto' }} {...elementStyle}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Nuncatsemfeugiatmassaiaculisvehiculaegetquisenim.Etiamsitametbibendumlacus.Infinibusquamnonportahendrerit.
        Integer non sem ipsum.
        Aenean congue nisl eu turpis tincidunt, in scelerisque est tempor.
        Phasellus hendrerit eros ac semper sodales.
      </Layout.Element>

      <Layout.Element selfSpacing="sm"><strong>['auto']</strong></Layout.Element>
      <Layout.Element overflow={['auto']} {...elementStyle}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Nuncatsemfeugiatmassaiaculisvehiculaegetquisenim.Etiamsitametbibendumlacus.Infinibusquamnonportahendrerit.
        Integer non sem ipsum.
        Aenean congue nisl eu turpis tincidunt, in scelerisque est tempor.
        Phasellus hendrerit eros ac semper sodales.
      </Layout.Element>

      <Layout.Element selfSpacing="sm"><strong>['hidden', 'auto']</strong></Layout.Element>
      <Layout.Element overflow={['hidden', 'auto']} {...elementStyle}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Nuncatsemfeugiatmassaiaculisvehiculaegetquisenim.Etiamsitametbibendumlacus.Infinibusquamnonportahendrerit.
        Integer non sem ipsum.
        Aenean congue nisl eu turpis tincidunt, in scelerisque est tempor.
        Phasellus hendrerit eros ac semper sodales.
      </Layout.Element>
    </Layout.Column>
  );
};


export const selfSpacing = () => (
  <Layout.Row spacedChildren="md" className="guide--blue guide--zebra-children" reversed={boolean('Reverse order in row', false)}>
    <Layout.Column spacedChildren="md" className="guide--zebra-children" reversed={boolean('Reverse order in column', false)}>
      <Layout.Element>Element 1a</Layout.Element>
      <Layout.Element selfSpacing="xl">Element 1b with own spacing</Layout.Element>
      <Layout.Element selfSpacing="none">Element 1c with no spacing</Layout.Element>
      <Layout.Element>Element 1d</Layout.Element>
    </Layout.Column>
    <Layout.Element selfSpacing="xl">Element 2 with own spacing</Layout.Element>
    <Layout.Element selfSpacing="none">Element 3 with no spacing</Layout.Element>
    <Layout.Element>Element 4</Layout.Element>
  </Layout.Row>
);


export const span = () => (
  <Layout.Column spacedChildren="lg">
    <Layout.Element selfSpacing="sm"><strong>span:auto</strong> - Variable width columns based on content size</Layout.Element>
    <Layout.Row className="guide--blue guide--zebra-children">
      <Layout.Element>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Layout.Element>
      <Layout.Element>Maecenas at lobortis nisl.</Layout.Element>
      <Layout.Element>Integer tincidunt lectus nunc...</Layout.Element>
    </Layout.Row>

    <Layout.Element selfSpacing="sm"><strong>span:greedy</strong> - Equal-width columns</Layout.Element>
    <Layout.Row className="guide--blue guide--zebra-children">
      <Layout.Element span="greedy">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Layout.Element>
      <Layout.Element span="greedy">Maecenas at lobortis nisl.</Layout.Element>
      <Layout.Element span="greedy">Integer tincidunt lectus nunc...</Layout.Element>
    </Layout.Row>

    <Layout.Element selfSpacing="sm"><strong>span:shrink</strong> - Only the big ones will shrink</Layout.Element>
    <Layout.Row className="guide--blue guide--zebra-children">
      <Layout.Element span="shrink">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Layout.Element>
      <Layout.Element span="shrink">Maecenas at lobortis nisl.</Layout.Element>
      <Layout.Element span="shrink">Integer tincidunt lectus nunc, eu malesuada metus tristique vel.</Layout.Element>
    </Layout.Row>

    <Layout.Element selfSpacing="sm"><strong>span:grow</strong> - Only the small ones will grow</Layout.Element>
    <Layout.Row className="guide--blue guide--zebra-children">
      <Layout.Element span="grow">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Layout.Element>
      <Layout.Element span="grow">Maecenas at lobortis nisl.</Layout.Element>
      <Layout.Element span="grow">Integer tincidunt...</Layout.Element>
    </Layout.Row>

    <Layout.Element selfSpacing="sm"><strong>span:1-12</strong> - Set column width based on 12 columns grid system</Layout.Element>
    <Layout.Column spacedChildren="sm">
      <Layout.Row className="guide--blue guide--zebra-children">
        <Layout.Element span="12">span:12</Layout.Element>
      </Layout.Row>
      <Layout.Row className="guide--blue guide--zebra-children">
        <Layout.Element span="6">span:6</Layout.Element>
        <Layout.Element span="6">span:6</Layout.Element>
      </Layout.Row>
      <Layout.Row className="guide--blue guide--zebra-children">
        <Layout.Element span="4">span:4</Layout.Element>
        <Layout.Element span="4">span:4</Layout.Element>
        <Layout.Element span="4">span:4</Layout.Element>
      </Layout.Row>
      <Layout.Row className="guide--blue guide--zebra-children">
        <Layout.Element span="3">span:3</Layout.Element>
        <Layout.Element span="3">span:3</Layout.Element>
        <Layout.Element span="3">span:3</Layout.Element>
        <Layout.Element span="3">span:3</Layout.Element>
      </Layout.Row>
      <Layout.Row className="guide--blue guide--zebra-children">
        <Layout.Element span="2">span:2</Layout.Element>
        <Layout.Element span="2">span:2</Layout.Element>
        <Layout.Element span="2">span:2</Layout.Element>
        <Layout.Element span="2">span:2</Layout.Element>
        <Layout.Element span="2">span:2</Layout.Element>
        <Layout.Element span="2">span:2</Layout.Element>
      </Layout.Row>
      <Layout.Row className="guide--blue guide--zebra-children">
        <Layout.Element span="1">span:1</Layout.Element>
        <Layout.Element span="1">span:1</Layout.Element>
        <Layout.Element span="1">span:1</Layout.Element>
        <Layout.Element span="1">span:1</Layout.Element>
        <Layout.Element span="1">span:1</Layout.Element>
        <Layout.Element span="1">span:1</Layout.Element>
        <Layout.Element span="1">span:1</Layout.Element>
        <Layout.Element span="1">span:1</Layout.Element>
        <Layout.Element span="1">span:1</Layout.Element>
        <Layout.Element span="1">span:1</Layout.Element>
        <Layout.Element span="1">span:1</Layout.Element>
        <Layout.Element span="1">span:1</Layout.Element>
      </Layout.Row>
    </Layout.Column>
  </Layout.Column>
);


export const withGutter = () => (
  <Layout.Row justifyContent="start" className="guide--blue guide--zebra-children">
    <Layout.Element withGutter="none">none</Layout.Element>
    <Layout.Element withGutter="xxs">xxs</Layout.Element>
    <Layout.Element withGutter="xs">xs</Layout.Element>
    <Layout.Element withGutter="sm">sm</Layout.Element>
    <Layout.Element withGutter="md">md</Layout.Element>
    <Layout.Element withGutter="lg">lg</Layout.Element>
    <Layout.Element withGutter="xl">xl</Layout.Element>
  </Layout.Row>
);

withGutter.story = {
  parameters: {
    docs: {
      storyDescription: 'Gutter is an uniform *horizontal* padding that can be added to the *layout element*.',
    },
  },
};


export const withPadding = () => (
  <Layout.Column spacedChildren="lg">

    <Layout.Element selfSpacing="sm"><strong>withPadding:LauotUnit</strong> - Use this variant to add uniform padding to the <em>layout element</em>.</Layout.Element>
    <Layout.Row justifyContent="start" alignItems="start" className="guide--blue guide--zebra-children">
      <Layout.Element withPadding="none">none</Layout.Element>
      <Layout.Element withPadding="xxs">xxs</Layout.Element>
      <Layout.Element withPadding="xs">xs</Layout.Element>
      <Layout.Element withPadding="sm">sm</Layout.Element>
      <Layout.Element withPadding="md">md</Layout.Element>
      <Layout.Element withPadding="lg">lg</Layout.Element>
      <Layout.Element withPadding="xl">xl</Layout.Element>
    </Layout.Row>

    <Layout.Element selfSpacing="sm"><strong>withPadding:LauotUnit[]</strong> - Use this variant to set paddings individually.</Layout.Element>
    <Layout.Element selfSpacing="sm"><strong><em>[vertical, horizontal]</em></strong></Layout.Element>
    <Layout.Row justifyContent="start" alignItems="start" className="guide--blue guide--zebra-children">
      <Layout.Element withPadding={['xl', 'sm']}>
        ['xl', 'sm']
      </Layout.Element>
      <Layout.Element withPadding={['sm', 'xl']}>
        ['sm', 'xl']
      </Layout.Element>
      <Layout.Element withPadding={['md', 'none']}>
        ['md', 'none']
      </Layout.Element>
      <Layout.Element withPadding={['none', 'md']}>
        ['none', 'md']
      </Layout.Element>
    </Layout.Row>

    <Layout.Element selfSpacing="sm"><strong><em>[top, right, bottom, left]</em></strong></Layout.Element>
    <Layout.Row justifyContent="start" alignItems="start" className="guide--blue guide--zebra-children">
      <Layout.Element withPadding={['xl', 'lg', 'md', 'sm']}>
        ['xl', 'lg', 'md', 'sm']
      </Layout.Element>
      <Layout.Element withPadding={['xl', 'lg', 'md']}>
        ['xl', 'lg', 'md']
      </Layout.Element>
      <Layout.Element withPadding={['xl', 'none', 'md']}>
        ['xl', 'none', 'md']
      </Layout.Element>
      <Layout.Element withPadding={['xl', 'lg']}>
        ['xl', 'lg']
      </Layout.Element>
      <Layout.Element withPadding={['xl']}>
        ['xl']
      </Layout.Element>
    </Layout.Row>

  </Layout.Column>
);
