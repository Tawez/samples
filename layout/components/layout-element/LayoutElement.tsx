// Libs
import React from 'react';


// Layout
import { joinClassNames } from 'layout/modules/bem';
import * as layout from 'layout/modules/layout';
import {
  WithNonInterferingComponent,
  createComponent,
} from 'layout/modules/utils';


// Module
type LayoutElementOwnProps = {
  className: string
  style: React.CSSProperties
};

export type LayoutElementProps<P extends object> = WithNonInterferingComponent<P, LayoutElementOwnProps> & React.HTMLAttributes<HTMLElement> & {
  alignSelf?: layout.AlignSelf
  className?: string
  elementRef?: React.Ref<any>
  /**
   * `number` or one of length data type: %, px, em, ex, vh, vw, vmin or vmax
   *
   * **NOTE:** If `height` is set, `span` will be forced to `"auto"`
   */
  height?: string | number
  overflow?: layout.Overflow
  selfSpacing?: layout.LayoutUnit
  span?: layout.Span
  style?: React.CSSProperties
  /**
   * `number` or one of length data type: %, px, em, ex, vh, vw, vmin or vmax
   *
   * **NOTE:** If `width` is set, span will be forced to `"auto"`
   */
  width?: string | number
  withGutter?: layout.LayoutUnit
  withPadding?: layout.Padding
};


/**
 * `LayoutElement` is a base layout component that can hold the content.
 *
 * ## Import
 *
 * ```
 * import { Layout } from 'layout/components';
 * ```
 *
 * And use `<Layout.Element />`
 */
export function LayoutElement<P extends object>({
  alignSelf,
  className,
  component = 'div',
  elementRef,
  height,
  overflow,
  selfSpacing,
  span = 'auto',
  style = {}, // to be able to pass custom style
  width,
  withGutter,
  withPadding,
  ...passProps
}: LayoutElementProps<P>): React.ReactElement {
  const isHeightSet = isSizeValueSet(height);
  const isWidthSet = isSizeValueSet(width);
  const isSizeSet = isHeightSet || isWidthSet;
  const ownProps: LayoutElementOwnProps = {
    className: joinClassNames(
      className,
      layout.spanClassName(isSizeSet ? 'auto' : span),
      layout.alignSelfClassName(alignSelf),
      layout.selfSpacingClassName(selfSpacing),
      layout.withGutterClassName(withGutter),
      layout.withPaddingClassName(withPadding),
      layout.overflowClassName(overflow),
    ),
    style,
  };

  if (isHeightSet) {
    ownProps.style.height = height;
  }
  if (isWidthSet) {
    ownProps.style.width = width;
  }

  const ref = typeof component == 'string'
    ? {ref: elementRef}
    : {elementRef};

  return createComponent(
    component,
    {
      ...ownProps,
      ...passProps,
      ...ref,
    },
  );
}


function isSizeValueSet(value?: string | number) {
  return !!value || value === 0;
}
