// Libs
import React from 'react';
import {
  boolean,
  select,
} from '@storybook/addon-knobs';


// Layout
import {
  alignContentKnob,
  alignItemsKnob,
  justifyContentKnob,
  layoutUnitKnob,
  spanKnob,
  wrapKnob,
} from 'layout/_doc_helpers/storybook';
import { Layout } from 'layout/components';


// Module
import { LayoutRow } from './LayoutRow';

export default {
  title: 'Layout System/LayoutRow',
  component: LayoutRow,
  excludeStories: /.*Data$/,
};


export const Playground = () => {
  const elementWidth = select('width', ['', '25%', '100px'], '', 'Elements');
  const elementSpan = spanKnob({ groupId: 'Elements'});

  return (
    <Layout.Row
      className="guide--gray-ee"
      height={select('height', ['', '200px'], '', 'Container')}
    >
      <Layout.Row
        className="guide--gray-cc"
        alignContent={alignContentKnob({ groupId: 'Row' })}
        alignItems={alignItemsKnob({ groupId: 'Row' })}
        justifyContent={justifyContentKnob({ groupId: 'Row' })}
        reversed={boolean('reversed', false, 'Row')}
        spacedChildren={layoutUnitKnob({ label: 'spacedChildren', groupId: 'Row' })}
        wrap={wrapKnob({ groupId: 'Row' })}
        span={spanKnob({ groupId: 'Row' })}
        withPadding={layoutUnitKnob({ label: 'withPadding', groupId: 'Row' })}
      >
        <Layout.Element className="guide--red" span={elementSpan} width={elementWidth}>element 1</Layout.Element>
        <Layout.Element className="guide--orange" height="40px" span={elementSpan} width={elementWidth} style={{ lineHeight: '2.5em' }}>element 2</Layout.Element>
        <Layout.Element className="guide--yellow" height="20px" span={elementSpan} width={elementWidth}>element 3</Layout.Element>
        <Layout.Element className="guide--green" height="80px" span={elementSpan} width={elementWidth} style={{ lineHeight: '6em' }}>element 4</Layout.Element>
        <Layout.Element className="guide--blue" height="60px" span={elementSpan} width={elementWidth}>element 5</Layout.Element>
      </Layout.Row>
    </Layout.Row>
  );
};


export const alignContent = () => (
  <Layout.Column spacedChildren="lg">
    <Layout.Row
      wrap="wrap"
      alignContent="normal"
      className="guide--blue guide--zebra-children"
      height="100px"
    >
      <Layout.Element width="30%">normal</Layout.Element>
      <Layout.Element width="30%">normal</Layout.Element>
      <Layout.Element width="30%">normal</Layout.Element>
      <Layout.Element width="30%">normal</Layout.Element>
    </Layout.Row>

    <Layout.Row
      wrap="wrap"
      alignContent="start"
      className="guide--blue guide--zebra-children"
      height="100px"
    >
      <Layout.Element width="30%">start</Layout.Element>
      <Layout.Element width="30%">start</Layout.Element>
      <Layout.Element width="30%">start</Layout.Element>
      <Layout.Element width="30%">start</Layout.Element>
    </Layout.Row>

    <Layout.Row
      wrap="wrap"
      alignContent="end"
      className="guide--blue guide--zebra-children"
      height="100px"
    >
      <Layout.Element width="30%">end</Layout.Element>
      <Layout.Element width="30%">end</Layout.Element>
      <Layout.Element width="30%">end</Layout.Element>
      <Layout.Element width="30%">end</Layout.Element>
    </Layout.Row>

    <Layout.Row
      wrap="wrap"
      alignContent="center"
      className="guide--blue guide--zebra-children"
      height="100px"
    >
      <Layout.Element width="30%">center</Layout.Element>
      <Layout.Element width="30%">center</Layout.Element>
      <Layout.Element width="30%">center</Layout.Element>
      <Layout.Element width="30%">center</Layout.Element>
    </Layout.Row>

    <Layout.Row
      wrap="wrap"
      alignContent="space-between"
      className="guide--blue guide--zebra-children"
      height="100px"
    >
      <Layout.Element width="30%">space-between</Layout.Element>
      <Layout.Element width="30%">space-between</Layout.Element>
      <Layout.Element width="30%">space-between</Layout.Element>
      <Layout.Element width="30%">space-between</Layout.Element>
    </Layout.Row>

    <Layout.Row
      wrap="wrap"
      alignContent="space-around"
      className="guide--blue guide--zebra-children"
      height="100px"
    >
      <Layout.Element width="30%">space-around</Layout.Element>
      <Layout.Element width="30%">space-around</Layout.Element>
      <Layout.Element width="30%">space-around</Layout.Element>
      <Layout.Element width="30%">space-around</Layout.Element>
    </Layout.Row>

    <Layout.Row
      wrap="wrap"
      alignContent="stretch"
      className="guide--blue guide--zebra-children"
      height="100px"
    >
      <Layout.Element width="30%">stretch</Layout.Element>
      <Layout.Element width="30%">stretch</Layout.Element>
      <Layout.Element width="30%">stretch</Layout.Element>
      <Layout.Element width="30%">stretch</Layout.Element>
    </Layout.Row>
  </Layout.Column>
);


export const alignItems = () => (
  <Layout.Column spacedChildren="md">
    <Layout.Row
      alignItems="start"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element height="40px" width="25%">start</Layout.Element>
      <Layout.Element height="20px" width="25%">start</Layout.Element>
      <Layout.Element height="80px" width="25%">start</Layout.Element>
      <Layout.Element height="60px" width="25%">start</Layout.Element>
    </Layout.Row>

    <Layout.Row
      alignItems="end"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element height="40px" width="25%">end</Layout.Element>
      <Layout.Element height="20px" width="25%">end</Layout.Element>
      <Layout.Element height="80px" width="25%">end</Layout.Element>
      <Layout.Element height="60px" width="25%">end</Layout.Element>
    </Layout.Row>

    <Layout.Row
      alignItems="center"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element height="40px" width="25%">center</Layout.Element>
      <Layout.Element height="20px" width="25%">center</Layout.Element>
      <Layout.Element height="80px" width="25%">center</Layout.Element>
      <Layout.Element height="60px" width="25%">center</Layout.Element>
    </Layout.Row>

    <Layout.Row
      alignItems="baseline"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element height="40px" width="25%" style={{ lineHeight: '2.5em' }}>baseline</Layout.Element>
      <Layout.Element height="20px" width="25%">baseline</Layout.Element>
      <Layout.Element height="80px" width="25%" style={{ lineHeight: '6em' }}>baseline</Layout.Element>
      <Layout.Element height="60px" width="25%">baseline</Layout.Element>
    </Layout.Row>

    <Layout.Row
      alignItems="stretch"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element width="25%">stretch</Layout.Element>
      <Layout.Element width="25%">stretch</Layout.Element>
      <Layout.Element height="80px" width="25%">stretch</Layout.Element>
      <Layout.Element width="25%">stretch</Layout.Element>
    </Layout.Row>
  </Layout.Column>
);


export const inline = () => (
  <Layout.Column spacedChildren="md">
    <Layout.Element>
      I <Layout.Row spacedChildren="xs" className="guide--blue guide--zebra-children">
        <span>am</span>
        <span>normal</span>
      </Layout.Row> Layout.Row
  </Layout.Element>

    <Layout.Element>
      I <Layout.Row spacedChildren="xs" inline className="guide--blue guide--zebra-children">
        <span>am</span>
        <span>inline</span>
      </Layout.Row> Layout.Row
  </Layout.Element>
  </Layout.Column>
);


export const justifyContent = () => (
  <Layout.Column spacedChildren="md">
    <Layout.Row
      justifyContent="start"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>start</Layout.Element>
      <Layout.Element>start</Layout.Element>
      <Layout.Element>start</Layout.Element>
    </Layout.Row>

    <Layout.Row
      justifyContent="end"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>end</Layout.Element>
      <Layout.Element>end</Layout.Element>
      <Layout.Element>end</Layout.Element>
    </Layout.Row>

    <Layout.Row
      justifyContent="center"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>center</Layout.Element>
      <Layout.Element>center</Layout.Element>
      <Layout.Element>center</Layout.Element>
    </Layout.Row>

    <Layout.Row
      justifyContent="space-between"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>space-between</Layout.Element>
      <Layout.Element>space-between</Layout.Element>
      <Layout.Element>space-between</Layout.Element>
    </Layout.Row>

    <Layout.Row
      justifyContent="space-around"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>space-around</Layout.Element>
      <Layout.Element>space-around</Layout.Element>
      <Layout.Element>space-around</Layout.Element>
    </Layout.Row>

    <Layout.Row
      justifyContent="space-evenly"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>space-evenly</Layout.Element>
      <Layout.Element>space-evenly</Layout.Element>
      <Layout.Element>space-evenly</Layout.Element>
    </Layout.Row>
  </Layout.Column>
);


export const reversed = () => (
  <Layout.Row reversed>
    <Layout.Element className="guide--green">element 1</Layout.Element>
    <Layout.Element className="guide--yellow">element 2</Layout.Element>
    <Layout.Element className="guide--orange">element 3</Layout.Element>
  </Layout.Row>
);


export const spacedChildren = () => (
  <Layout.Column spacedChildren="md">
    <Layout.Row className="guide--blue guide--zebra-children">
      <Layout.Element>none</Layout.Element>
      <Layout.Element>none</Layout.Element>
      <Layout.Element>none</Layout.Element>
    </Layout.Row>

    <Layout.Row
      spacedChildren="xxs"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>xxs</Layout.Element>
      <Layout.Element>xxs</Layout.Element>
      <Layout.Element>xxs</Layout.Element>
    </Layout.Row>

    <Layout.Row
      spacedChildren="xs"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>xs</Layout.Element>
      <Layout.Element>xs</Layout.Element>
      <Layout.Element>xs</Layout.Element>
    </Layout.Row>

    <Layout.Row
      spacedChildren="sm"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>sm</Layout.Element>
      <Layout.Element>sm</Layout.Element>
      <Layout.Element>sm</Layout.Element>
    </Layout.Row>

    <Layout.Row
      spacedChildren="md"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>md</Layout.Element>
      <Layout.Element>md</Layout.Element>
      <Layout.Element>md</Layout.Element>
    </Layout.Row>

    <Layout.Row
      spacedChildren="lg"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>lg</Layout.Element>
      <Layout.Element>lg</Layout.Element>
      <Layout.Element>lg</Layout.Element>
    </Layout.Row>

    <Layout.Row
      spacedChildren="xl"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>xl</Layout.Element>
      <Layout.Element>xl</Layout.Element>
      <Layout.Element>xl</Layout.Element>
    </Layout.Row>
  </Layout.Column>
);


export const wrap = () => (
  <Layout.Column spacedChildren="lg">
    <Layout.Element selfSpacing="sm"><strong>nowrap</strong></Layout.Element>
    <Layout.Row
      wrap="nowrap"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element width="30%">element 1</Layout.Element>
      <Layout.Element width="30%">element 2</Layout.Element>
      <Layout.Element width="30%">element 3</Layout.Element>
      <Layout.Element width="30%">element 4</Layout.Element>
    </Layout.Row>

    <Layout.Element selfSpacing="sm"><strong>wrap</strong></Layout.Element>
    <Layout.Row
      wrap="wrap"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element width="30%">element 1</Layout.Element>
      <Layout.Element width="30%">element 2</Layout.Element>
      <Layout.Element width="30%">element 3</Layout.Element>
      <Layout.Element width="30%">element 4</Layout.Element>
    </Layout.Row>

    <Layout.Element selfSpacing="sm"><strong>wrap-reverse</strong></Layout.Element>
    <Layout.Row
      wrap="wrap-reverse"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element width="30%">element 1</Layout.Element>
      <Layout.Element width="30%">element 2</Layout.Element>
      <Layout.Element width="30%">element 3</Layout.Element>
      <Layout.Element width="30%">element 4</Layout.Element>
    </Layout.Row>
  </Layout.Column>
);
