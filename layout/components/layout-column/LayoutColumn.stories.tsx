// Libs
import React from 'react';
import { boolean } from '@storybook/addon-knobs';


// Layout
import {
  alignItemsKnob,
  justifyContentKnob,
  layoutUnitKnob,
  spanKnob,
} from 'layout/_doc_helpers/storybook';
import { Layout } from 'layout/components';


// Module
import { LayoutColumn } from './LayoutColumn';

export default {
  title: 'Layout System/LayoutColumn',
  component: LayoutColumn,
  excludeStories: /.*Data$/,
};


export const Playground = () => (
  <Layout.Column height="200px" className="guide--gray-ee">
    <Layout.Column
      className="guide--gray-cc"
      alignItems={alignItemsKnob()}
      justifyContent={justifyContentKnob()}
      reversed={boolean('reversed', false)}
      spacedChildren={layoutUnitKnob({ label: 'spacedChildren' })}
      span={spanKnob()}
      withPadding={layoutUnitKnob({ label: 'withPadding'})}
    >
      <Layout.Element className="guide--red">element 1</Layout.Element>
      <Layout.Element className="guide--orange">element 2</Layout.Element>
      <Layout.Element className="guide--yellow">element 3</Layout.Element>
      <Layout.Element className="guide--green">element 4</Layout.Element>
      <Layout.Element className="guide--blue">element 5</Layout.Element>
    </Layout.Column>
  </Layout.Column>
);


export const alignItems = () => (
  <Layout.Row spacedChildren="md">
    <Layout.Column
      alignItems="start"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>start</Layout.Element>
      <Layout.Element width="75%">start</Layout.Element>
      <Layout.Element width="50%">start</Layout.Element>
    </Layout.Column>

    <Layout.Column
      alignItems="end"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>end</Layout.Element>
      <Layout.Element width="75%">end</Layout.Element>
      <Layout.Element width="50%">end</Layout.Element>
    </Layout.Column>

    <Layout.Column
      alignItems="center"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>center</Layout.Element>
      <Layout.Element width="75%">center</Layout.Element>
      <Layout.Element width="50%">center</Layout.Element>
    </Layout.Column>

    <Layout.Column
      alignItems="stretch"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>stretch</Layout.Element>
      <Layout.Element>stretch</Layout.Element>
      <Layout.Element>stretch</Layout.Element>
    </Layout.Column>
  </Layout.Row>
);

alignItems.story = {
  parameters: {
    docs: {
      storyDescription: '**NOTE:** `baseline` is a valid value for `alignItems`, but has no effect in *LayoutColumn*.',
    },
  },
};


export const justifyContent = () => (
  <Layout.Row height="150px" spacedChildren="md">
    <Layout.Column
      justifyContent="start"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>start</Layout.Element>
      <Layout.Element>start</Layout.Element>
      <Layout.Element>start</Layout.Element>
    </Layout.Column>

    <Layout.Column
      justifyContent="end"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>end</Layout.Element>
      <Layout.Element>end</Layout.Element>
      <Layout.Element>end</Layout.Element>
    </Layout.Column>

    <Layout.Column
      justifyContent="center"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>center</Layout.Element>
      <Layout.Element>center</Layout.Element>
      <Layout.Element>center</Layout.Element>
    </Layout.Column>

    <Layout.Column
      justifyContent="space-between"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>space-between</Layout.Element>
      <Layout.Element>space-between</Layout.Element>
      <Layout.Element>space-between</Layout.Element>
    </Layout.Column>

    <Layout.Column
      justifyContent="space-around"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>space-around</Layout.Element>
      <Layout.Element>space-around</Layout.Element>
      <Layout.Element>space-around</Layout.Element>
    </Layout.Column>

    <Layout.Column
      justifyContent="space-evenly"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>space-evenly</Layout.Element>
      <Layout.Element>space-evenly</Layout.Element>
      <Layout.Element>space-evenly</Layout.Element>
    </Layout.Column>
  </Layout.Row>
);


export const reversed = () => (
  <Layout.Column reversed>
    <Layout.Element className="guide--green">element 1</Layout.Element>
    <Layout.Element className="guide--yellow">element 2</Layout.Element>
    <Layout.Element className="guide--orange">element 3</Layout.Element>
  </Layout.Column>
);


export const spacedChildren = () => (
  <Layout.Row height={140} spacedChildren="md">
    <Layout.Column className="guide--blue guide--zebra-children">
      <Layout.Element>none</Layout.Element>
      <Layout.Element>none</Layout.Element>
      <Layout.Element>none</Layout.Element>
    </Layout.Column>

    <Layout.Column
      spacedChildren="xxs"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>xxs</Layout.Element>
      <Layout.Element>xxs</Layout.Element>
      <Layout.Element>xxs</Layout.Element>
    </Layout.Column>

    <Layout.Column
      spacedChildren="xs"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>xs</Layout.Element>
      <Layout.Element>xs</Layout.Element>
      <Layout.Element>xs</Layout.Element>
    </Layout.Column>

    <Layout.Column
      spacedChildren="sm"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>sm</Layout.Element>
      <Layout.Element>sm</Layout.Element>
      <Layout.Element>sm</Layout.Element>
    </Layout.Column>

    <Layout.Column
      spacedChildren="md"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>md</Layout.Element>
      <Layout.Element>md</Layout.Element>
      <Layout.Element>md</Layout.Element>
    </Layout.Column>

    <Layout.Column
      spacedChildren="lg"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>lg</Layout.Element>
      <Layout.Element>lg</Layout.Element>
      <Layout.Element>lg</Layout.Element>
    </Layout.Column>

    <Layout.Column
      spacedChildren="xl"
      className="guide--blue guide--zebra-children"
    >
      <Layout.Element>xl</Layout.Element>
      <Layout.Element>xl</Layout.Element>
      <Layout.Element>xl</Layout.Element>
    </Layout.Column>
  </Layout.Row>
);
