// Libs
import React from 'react';


// Layout
import { bemBlock } from 'layout/modules/bem';


// Module
import './LayoutSeparator.less';

const block = bemBlock('n-LayoutSeparator');

export type LayoutSeparatorProps = {
  className?: string
}


/**
 * `LayoutSeparator` is a helper that may be used within[LayoutColumn](#layoutcolumn) and[LayoutRow](#layoutrow).
 *
 * ## Import
 *
 * ```
 * import { Layout } from 'layout/components';
 * ```
 *
 * And use`<Layout.Separator />`.
 */
export const LayoutSeparator: React.FC<LayoutSeparatorProps> = ({
  className,
}) => {
  const ownProps = {
    className: block({extra: className}),
  };

  return <div {...ownProps} />;
};
