// Libs
import React from 'react';


// Layout
import { Layout } from 'layout/components';


// Module
import { LayoutSeparator } from './LayoutSeparator';

export default {
  title: 'Layout System/LayoutSeparator',
  component: LayoutSeparator,
  excludeStories: /.*Data$/,
};


export const RowExample = () => (
  <Layout.Row spacedChildren="md">
    <Layout.Element>Item 1</Layout.Element>
    <Layout.Element>Item 2</Layout.Element>
    <Layout.Separator />
    <Layout.Element>Item 3</Layout.Element>
    <Layout.Element>Item 4</Layout.Element>
    <Layout.Fill />
    <Layout.Separator />
    <Layout.Element>Item 5</Layout.Element>
  </Layout.Row>
);


export const ColumnExample = () => (
  <Layout.Column spacedChildren="xs" height="200px">
    <Layout.Element>Item 1</Layout.Element>
    <Layout.Element>Item 2</Layout.Element>
    <Layout.Separator />
    <Layout.Element>Item 3</Layout.Element>
    <Layout.Element>Item 4</Layout.Element>
    <Layout.Fill />
    <Layout.Separator />
    <Layout.Element>Item 5</Layout.Element>
  </Layout.Column>
);
