import { LayoutColumn } from 'layout/components/layout-column/LayoutColumn';
import { LayoutElement } from 'layout/components/layout-element/LayoutElement';
import { LayoutFill } from 'layout/components/layout-fill/LayoutFill';
import { LayoutRow } from 'layout/components/layout-row/LayoutRow';
import { LayoutSeparator } from 'layout/components/layout-separator/LayoutSeparator';


// Module
export class Layout {
  static Column = LayoutColumn;
  static Element = LayoutElement;
  static Fill = LayoutFill;
  static Row = LayoutRow;
  static Separator = LayoutSeparator;
}
