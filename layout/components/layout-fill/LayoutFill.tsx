// Libs
import React from 'react';


// Layout
import { joinClassNames } from 'layout/modules/bem';
import * as layout from 'layout/modules/layout';


// Module
export type LayoutFillProps = {
  className?: string,
}

/**
 * `LayoutFill` is a helper that may be used within `LayoutColumn` and `LayoutRow`.
 * It can be used to divide elements into equally spaced groups.
 *
 * ## Import
 *
 * ```
 * import { Layout } from 'layout/components';
 * ```
 *
 * And use `<Layout.Fill />`.
 */
export const LayoutFill: React.FC<LayoutFillProps> = ({
  className,
}) => {
  const props = {
    className: joinClassNames(
      layout.spanClassName('greedy'),
      className,
    ),
    'data-ui-role': 'layout-fill',
  };

  return <div {...props} />;
};
