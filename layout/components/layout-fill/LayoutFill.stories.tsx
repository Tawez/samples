// Libs
import React from 'react';


// Layout
import { Layout } from 'layout/components';


// Module
import { LayoutFill } from './LayoutFill';


export default {
  title: 'Layout System/LayoutFill',
  component: LayoutFill,
  excludeStories: /.*Data$/,
};


export const RowExample = () => (
  <Layout.Row className="guide--gray-cc">
    <Layout.Element className="guide--blue">Item 1</Layout.Element>
    <Layout.Element className="guide--green">Item 2</Layout.Element>
    <Layout.Fill />
    <Layout.Element className="guide--yellow">Item 3</Layout.Element>
    <Layout.Element className="guide--orange">Item 4</Layout.Element>
    <Layout.Fill />
    <Layout.Element className="guide--red">Item 5</Layout.Element>
  </Layout.Row>
);


export const ColumnExample = () => (
  <Layout.Column height="200px" className="guide--gray-cc">
    <Layout.Element className="guide--blue">Item 1</Layout.Element>
    <Layout.Element className="guide--green">Item 2</Layout.Element>
    <Layout.Fill />
    <Layout.Element className="guide--yellow">Item 3</Layout.Element>
    <Layout.Element className="guide--orange">Item 4</Layout.Element>
    <Layout.Fill />
    <Layout.Element className="guide--red">Item 5</Layout.Element>
  </Layout.Column>
);
