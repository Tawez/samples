// Libs
import { select } from '@storybook/addon-knobs';


// Layout
import * as layout from 'layout/modules/layout';


// Module

// {{{ Helpers
export type OptionalSelectValue<T> = { [K: string]: T | undefined };
export interface KnobInterface<T> {
  label?: string,
  value?: T,
  groupId?: string
}
// }}} Helpers


// {{{ layoutUnitKnob
const layoutUnitValues: OptionalSelectValue<layout.LayoutUnit> = {
  '[undefined]': undefined,
  'none (0px)': 'none',
  'xxs (2px)': 'xxs',
  'xs (4px)': 'xs',
  'sm (8px)': 'sm',
  'md (16px)': 'md',
  'lg (24px)': 'lg',
  'xl (32px)': 'xl',
};

type LayoutUnitKnobParams = KnobInterface<layout.LayoutUnit>

export function layoutUnitKnob({ label = 'Layout unit', value = undefined, groupId = undefined }: LayoutUnitKnobParams = {}) {
  return select(label, layoutUnitValues, value, groupId) || undefined;
}
// }}} layoutUnitKnob


// {{{ spanKnob
const spanValues: OptionalSelectValue<layout.Span> = {
  '[undefined]': undefined,
  'greedy': 'greedy',
  'auto': 'auto',
  'grow': 'grow',
  'shrink': 'shrink',
  '1': '1',
  '2': '2',
  '3': '3',
  '4': '4',
  '5': '5',
  '6': '6',
  '7': '7',
  '8': '8',
  '9': '9',
  '10': '10',
  '11': '11',
  '12': '12',
};

type SpanKnobParams = KnobInterface<layout.Span>;

export function spanKnob({ label = 'span', value = undefined, groupId = undefined}: SpanKnobParams = {}) {
  return select(label, spanValues, value, groupId) || undefined;
}
// }}} spanKnob


// {{{ alignSelfKnob
const alignSelfValues: OptionalSelectValue<layout.AlignSelf> = {
  '[undefined]': undefined,
  'start': 'start',
  'end': 'end',
  'center': 'center',
  'baseline': 'baseline',
  'stretch': 'stretch',
  'auto': 'auto',
};

type AlignSelfKnobParams = KnobInterface<layout.AlignSelf>;

export function alignSelfKnob({ label = 'alignSelf', value = undefined, groupId = undefined }: AlignSelfKnobParams = {}) {
  return select(label, alignSelfValues, value, groupId) || undefined;
}
// }}} alignSelfKnob


// {{{ overflowKnob
const overflowValues: OptionalSelectValue<layout.OverflowBasicValue> = {
  '[undefined]': undefined,
  'auto': 'auto',
  'hidden': 'hidden',
  'scroll': 'scroll',
  'visible': 'visible',
};

type OverflowKnobParams = KnobInterface<layout.OverflowBasicValue>;

export function overflowKnob({ label = 'overflow', value = undefined, groupId = undefined }: OverflowKnobParams = {}) {
  return select(label, overflowValues, value, groupId) || undefined;
}
// }}} overflowKnob


// {{{ alignItemsKnob
const alignItemsValues: OptionalSelectValue<layout.AlignItems> = {
  '[undefined]': undefined,
  'start': 'start',
  'end': 'end',
  'center': 'center',
  'baseline': 'baseline',
  'stretch': 'stretch',
};

type AlignItemsKnobParams = KnobInterface<layout.AlignItems>;

export function alignItemsKnob({ label = 'alignItems', value = undefined, groupId = undefined }: AlignItemsKnobParams = {}) {
  return select(label, alignItemsValues, value, groupId) || undefined;
}
// }}} alignItemsKnob


// {{{ justifyContentKnob
const justifyContentValues: OptionalSelectValue<layout.JustifyContent> = {
  '[undefined]': undefined,
  'start': 'start',
  'end': 'end',
  'center': 'center',
  'space-between': 'space-between',
  'space-around': 'space-around',
  'space-evenly': 'space-evenly',
};

type JustifyContentKnobParams = KnobInterface<layout.JustifyContent>;

export function justifyContentKnob({ label = 'justifyContent', value = undefined, groupId = undefined }: JustifyContentKnobParams = {}) {
  return select(label, justifyContentValues, value, groupId) || undefined;
}
// }}} justifyContentKnob


// {{{ alignContentKnob
const alignContentValues: OptionalSelectValue<layout.AlignContent> = {
  '[undefined]': undefined,
  'normal': 'normal',
  'start': 'start',
  'end': 'end',
  'center': 'center',
  'space-between': 'space-between',
  'space-around': 'space-around',
  'stretch': 'stretch',
};

type AlignContentKnobParams = KnobInterface<layout.AlignContent>;

export function alignContentKnob({ label = 'alignContent', value = undefined, groupId = undefined }: AlignContentKnobParams = {}) {
  return select(label, alignContentValues, value, groupId) || undefined;
}
// }}} alignContentKnob


// {{{ wrapKnob
const wrapValues: OptionalSelectValue<layout.Wrap> = {
  '[undefined]': undefined,
  'nowrap': 'nowrap',
  'wrap': 'wrap',
  'wrap-reverse': 'wrap-reverse',
};

type WrapKnobParams = KnobInterface<layout.Wrap>;

export function wrapKnob({ label = 'wrap', value = undefined, groupId = undefined }: WrapKnobParams = {}) {
  return select(label, wrapValues, value, groupId) || undefined;
}
// }}} wrapKnob
