// Libs
import React from 'react';

// Module
export type TComponent = string | React.ComponentType<any>;
export type WithComponent = React.PropsWithChildren<{ component?: TComponent }>;
export type WithoutInterferers<T, I> = Omit<T, keyof I>
export type WithNonInterferingComponent<T, I> = WithoutInterferers<T, I> & WithComponent;

export function createComponent(component: TComponent, props?: any, ...children: React.ReactNode[]) {
  return React.createElement(
    component,
    props,
    ...children,
  );
}
