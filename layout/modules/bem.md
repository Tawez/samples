### `joinClassNames`

Joins given strings. Ignores empty and falsy values.


#### Syntax

```ts
joinClassNames(...classes: Array<string | null | undefined>): string
```

#### Usage

```js static
import { joinClassNames } from 'layout/modules/bem';

joinClassNames('Ala', '', '  ', undefined, null, 'Makota'); // => 'Ala Makota'
```


### class `BEM`

Class supports [BEM](https://en.bem.info/) methodology.

#### Syntax

```ts
type EntryValue = string | number | boolean | null | undefined;
type Entries = { [key: string]: EntryValue };
type RawEntry = Entries | EntryValue;
type RawEntires = RawEntry[] | Entries | string;

class BEM {
  constructor(block: string, element?: string, modifiers?: RawEntires, extra?: RawEntires): BEM

  add(...entries: RawEntry[]): this

  el(element: string, modifiers?: RawEntires, extra?: RawEntires): BEM

  is(...entries: RawEntry[]): this

  toString(): string
}
```

#### Usage

```js static
import { BEM } from 'layout/modules/bem';

const block = new BEM('block');

const MyComponent = ({
  className
}) => {
  return (
    <div className={block.add(className).toString()}>MyComponent</div>
  );
}
```

#### Interface

##### `constructor(block: string, element?: string, modifiers?: RawEntires, extra?: RawEntires)`

Create a block or element with optional modifiers and extra classes.

```js static
new BEM('block').toString() // => 'block'

new BEM('block', 'element').toString() // => 'block__element'

new BEM('block', 'element', 'modifier').toString() // => 'block__element block__element--modifier'
new BEM('block', 'element', ['modifier']).toString() // => 'block__element block__element--modifier'
new BEM('block', 'element', {'modifier': true}).toString() // => 'block__element block__element--modifier'

new BEM('block', 'element', undefined, 'extra').toString() // => 'block__element extra'
new BEM('block', 'element', undefined, ['extra']).toString() // => 'block__element extra'
new BEM('block', 'element', undefined, {'extra': true}).toString() // => 'block__element extra'

new BEM('block', 'element', 'modifier', 'extra').toString() // => 'block__element block__element--modifier extra'
```

> **NOTE:**
> Empty `element` is not allowed and will be ignored


##### `add(...entries: RawEntry[])`

Add extra classes. Chainable.

```js static
new BEM('block')
  .add(
    ' Ala ',
    'foo bar',
    '  ',
    '',
    123,
    0,
    null,
    false,
    undefined,
    {
      ' one ': ' ONE ',
      'two': 123,
      'three four': true,
      'five': 0,
      'six': '',
      'seven': '  ',
      'eight': false,
      'nine': null,
      'ten': undefined,
    }
  )
  .toString() // => 'block Ala foo bar 123 0 one two three four five'
```

> **NOTE:**
> Empty keys will be ignored

> **NOTE:**
> Keys will be trimmed

> **NOTE:**
> Keys will be split into single words

> **NOTE:**
> Keys with empty values (*''*, *'   '*, *false*, *null*, *undefined*) will be ignored

Can be invoked many times.

```js static
new BEM('block')
  .add({ 'foo': true, 'bar': true })
  .add({ 'foo': false })
  .toString() // => 'block bar'
```


##### `el(element: string, modifiers?: RawEntires, extra?: RawEntires)`

Create new element

```js static
const element1 = new BEM('block', 'element1', 'foo', 'bar');

element1.el('element2').toString() // => 'block__element2'

element1.el('element2', 'baz').toString() // => 'block__element2 block__element2--baz'
element1.el('element2', ['baz']).toString() // => 'block__element2 block__element2--baz'
element1.el('element2', {'baz': true}).toString() // => 'block__element2 block__element2--baz'

element1.el('element2', undefined, 'buz').toString() // => 'block__element2 buz'
element1.el('element2', undefined, ['buz']).toString() // => 'block__element2 buz'
element1.el('element2', undefined, {'buz':true}).toString() // => 'block__element2 buz'

element1.el('element2', 'baz', 'buz').toString() // => 'block__element2 block__element2--baz buz'
```

> **NOTE:**
> `el` returns new BEM instance and does not copy modifiers and extra classes.


##### `is(...entries: RawEntry[])`

Set modifiers. Chainable.

```js static
new BEM('block')
  .is(
    ' Ala ',
    'foo bar',
    '  ',
    '',
    123,
    0,
    null,
    false,
    undefined,
    {
      ' one ': ' ONE ',
      'two': 123,
      'three four': true,
      'five': 0,
      'six': '',
      'seven': '  ',
      'eight': false,
      'nine': null,
      'ten': undefined,
    }
  )
  .toString() // => 'block block--Ala block--foo block--bar block--123 block--0 block--one-ONE block--two-123 block--three block--four block--five-0'
```

> **NOTE:**
> Empty keys will be ignored

> **NOTE:**
> Keys and values will be trimmed

> **NOTE:**
> Keys will be split to single words

> **NOTE:**
> Keys with empty values (*''*, *'   '*, *false*, *null*, *undefined*) will be ignored

Can be invoked many times.

```js static
new BEM('block')
  .is({ 'foo': 123, 'bar': 'bar', 'baz buz': true })
  .is({ 'foo': false, 'bar': 'BAR', 'buz': false})
  .toString() // => 'block block--bar-BAR block--baz'
```


##### `toString()`

Return string representation of BEM instance.

```jsx
import { BEM } from 'layout/modules/bem';

const block = new BEM('block');

block.add('guide--blue', 'guide--zebra-children');

<div>
  <div className={block.toString()}>
    <span>one</span>
    <span>two</span>
    <span>three</span>
    <span>four</span>
  </div>
  <div className={block.add(
    'guide--red',
    {'guide--blue': false}
  ).toString()}>
    <span>one</span>
    <span>two</span>
    <span>three</span>
    <span>four</span>
  </div>
</div>
```


### `bemBlock`

Creates helper function for BEM block.

Created function, when called, returns *String* with CSS classes.
The set of classes depends on arguments passed to the function.


#### Syntax

```ts
bemBlock(block: string): BlockHelper

type BlockHelper = (options?: ElementDefinition) => string;

type ElementDefinition = string | {
  element?: string,
  modifiers?: RawEntires,
  extra?: RawEntires,
};
```

For `RawEntries` definition see [BEM class](#class-bem).


#### Usage

```js static
import { bemBlock } from 'layout/modules/bem';

const block = bemBlock('block');

const MyComponent = ({
  className,
  light
}) => {
  return (
    <div className={block({extra: className})}>
      <span className={block('title')}>MyComponent</span>
      <span className={block({
        element: 'description',
        modifiers: { light }
      })}>is awesome</span>
    </div>
  );
}
```
