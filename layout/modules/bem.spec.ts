import {
  BEM,
  bemBlock,
  joinClassNames,
} from './bem';


describe('joinClassNames', () => {
  it('concatenates given strings', () => {
    expect(
      joinClassNames('Ala', 'Makota'),
    ).toBe('Ala Makota');
  });

  it('omits falsy and empty values', () => {
    expect(
      joinClassNames('Ala', '', '  ', undefined, null, 'Makota'),
    ).toBe('Ala Makota');
  });
});


describe('BEM', () => {
  describe('constructor', () => {
    it('allows to define block', () => {
      expect(
        new BEM('block').baseClassName,
      ).toBe('block');
    });

    it('allows to define element', () => {
      expect(
        new BEM('block', 'element').baseClassName,
      ).toBe('block__element');
    });

    it('does not allow empty element', () => {
      expect(
        new BEM('block', '').baseClassName,
      ).toBe('block');
      expect(
        new BEM('block', '  ').baseClassName,
      ).toBe('block');
    });

    it('allows to add modifiers', () => {
      expect(
        new BEM('block', undefined, 'foo').modifierClassNames,
      ).toEqual(['block--foo']);
      expect(
        new BEM('block', undefined, ['foo']).modifierClassNames,
      ).toEqual(['block--foo']);
      expect(
        new BEM('block', undefined, {'foo': 'FOO'}).modifierClassNames,
      ).toEqual(['block--foo-FOO']);
    });

    it('allows to add extra classes', () => {
      expect(
        new BEM('block', undefined, undefined, 'bar').extraClassNames,
      ).toEqual(['bar']);
      expect(
        new BEM('block', undefined, undefined, ['bar']).extraClassNames,
      ).toEqual(['bar']);
      expect(
        new BEM('block', undefined, undefined, {'bar': 'BAR'}).extraClassNames,
      ).toEqual(['bar']);
    });
  });

  describe('add', () => {
    it('adds non falsy nor empty extra classes', () => {
      expect(
        new BEM('block').add(
          'foo',
          '  ',
          '',
          123,
          0,
          null,
          false,
          undefined,
        ).extraClassNames,
      ).toEqual(['0', '123', 'foo']);
    });

    it('splits extra classes given as string', () => {
      expect(
        new BEM('block').add('foo bar').extraClassNames,
      ).toEqual(['foo', 'bar']);
    });

    it('trims given keys', () => {
      expect(
        new BEM('block').add(' foo ', {' bar ': true}).extraClassNames,
      ).toEqual(['foo', 'bar']);
    });

    it('adds extra classes given as object of keys with non falsy nor empty values', () => {
      expect(
        new BEM('block').add({
          'one': 'non-empty',
          'two': 123,
          'three four': true,
          'five': 0,
          'six': '',
          'seven': '  ',
          'eight': false,
          'nine': null,
          'ten': undefined,
        }).extraClassNames,
      ).toEqual(['one', 'two', 'three', 'four', 'five']);
    });

    it('updates extra classes', () => {
      expect(
        new BEM('block')
          .add({ 'foo': true, 'bar': true })
          .add({ 'foo': false })
          .extraClassNames,
      ).toEqual(['bar']);
    });
  });

  describe('el', () => {
    it('returns new BEM instance', () => {
      const block = new BEM('block');
      const element = block.el('element');

      expect(block === element).toBe(false);
    });

    it('sets element to given', () => {
      expect(
        new BEM('block', 'element1').el('element2').baseClassName,
      ).toBe('block__element2');
    });

    it('does not set empty element', () => {
      expect(
        new BEM('block', 'element1').el('').baseClassName,
      ).toBe('block');
      expect(
        new BEM('block', 'element1').el('  ').baseClassName,
      ).toBe('block');
    });

    it('does not copy modifiers nor extras', () => {
      const element1 = new BEM('block', 'element1', 'foo', 'bar');
      const element2 = element1.el('element2');

      expect(element2.modifierClassNames).toEqual([]);
      expect(element2.extraClassNames).toEqual([]);
    });

    it('sets new modifiers and extras', () => {
      const element1 = new BEM('block', 'element1', 'foo', 'bar');
      const element2 = element1.el('element2', 'baz', 'buz');

      expect(element2.baseClassName).toBe('block__element2');
      expect(element2.modifierClassNames).toEqual(['block__element2--baz']);
      expect(element2.extraClassNames).toEqual(['buz']);
    });
  });

  describe('is', () => {
    it('adds non falsy nor empty modifiers', () => {
      expect(
        new BEM('block').is(
          'foo',
          '  ',
          '',
          123,
          0,
          null,
          false,
          undefined,
        ).modifierClassNames,
      ).toEqual(['block--0', 'block--123', 'block--foo']);
      expect(
        new BEM('block', 'element').is(
          'foo',
          '  ',
          '',
          123,
          0,
          null,
          false,
          undefined,
        ).modifierClassNames,
      ).toEqual(['block__element--0', 'block__element--123', 'block__element--foo']);
    });

    it('splits modifiers given as string', () => {
      expect(
        new BEM('block').is('foo bar').modifierClassNames,
      ).toEqual(['block--foo', 'block--bar']);
      expect(
        new BEM('block', 'element').is('foo bar').modifierClassNames,
      ).toEqual(['block__element--foo', 'block__element--bar']);
    });

    it('trims given keys and values', () => {
      expect(
        new BEM('block').is(' foo ', { ' bar ': ' BAR ' }).modifierClassNames,
      ).toEqual(['block--foo', 'block--bar-BAR']);
      expect(
        new BEM('block', 'element').is(' foo ', { ' bar ': ' BAR ' }).modifierClassNames,
      ).toEqual(['block__element--foo', 'block__element--bar-BAR']);
    });

    it('adds modifiers given as object of keys with non falsy nor empty values', () => {
      const classes = {
        'one': 'empty',
        'two': 123,
        'three four': true,
        'five': 0,
        'six': '',
        'seven': '  ',
        'eight': false,
        'nine': null,
        'ten': undefined,
      };
      expect(
        new BEM('block').is(classes).modifierClassNames,
      ).toEqual(['block--one-empty', 'block--two-123', 'block--three', 'block--four', 'block--five-0']);
      expect(
        new BEM('block', 'element').is(classes).modifierClassNames,
      ).toEqual(['block__element--one-empty', 'block__element--two-123', 'block__element--three', 'block__element--four', 'block__element--five-0']);
    });

    it('updates modifiers', () => {
      expect(
        new BEM('block')
          .is({ 'foo': 123, 'bar': 'bar', 'baz buz': true })
          .is({ 'foo': false, 'bar': 'BAR', 'buz': false})
          .modifierClassNames,
      ).toEqual(['block--bar-BAR', 'block--baz']);
      expect(
        new BEM('block', 'element')
          .is({ 'foo': 123, 'bar': 'bar', 'baz buz': true })
          .is({ 'foo': false, 'bar': 'BAR', 'buz': false })
          .modifierClassNames,
      ).toEqual(['block__element--bar-BAR', 'block__element--baz']);
    });
  });

  describe('toString', () => {
    it('returns all classes for given BEM instance', () => {
      expect(
        new BEM('block', 'element')
          .is('foo', { bar: 'BAR' })
          .add('baz')
          .toString(),
      ).toBe('block__element block__element--foo block__element--bar-BAR baz');
    });
  });
});


describe('bemBlock', () => {
  it('should return function', () => {
    expect(
      bemBlock('block'),
    ).toBeInstanceOf(Function);
  });

  it('defined function should return string', () => {
    expect(
      typeof bemBlock('block')(),
    ).toEqual('string');
  });
});


describe('function returned by bemBlock', () => {

  const block = bemBlock('block');

  it('should return class for block when no arguments given', () => {
    expect(
      block(),
    ).toEqual('block');
    expect(
      block({}),
    ).toEqual('block');
  });

  it('should return class for element when element given', () => {
    expect(
      block('element'),
    ).toEqual('block__element');
    expect(
      block({ element: 'element' }),
    ).toEqual('block__element');
  });

  it('should return class for block when given element is empty string', () => {
    expect(
      block(''),
    ).toEqual('block');
    expect(
      block('  '),
    ).toEqual('block');
    expect(
      block({ element: '' }),
    ).toEqual('block');
    expect(
      block({ element: '  ' }),
    ).toEqual('block');
  });

  it('should add modifier given as string', () => {
    expect(
      block({ modifiers: 'modifier' }),
    ).toEqual('block block--modifier');
    expect(
      block({
        element: 'element',
        modifiers: 'modifier',
      }),
    ).toEqual('block__element block__element--modifier');
  });

  it('should split modifiers given as string', () => {
    expect(
      block({ modifiers: 'one two' }),
    ).toEqual('block block--one block--two');
    expect(
      block({
        element: 'element',
        modifiers: 'one two',
      }),
    ).toEqual('block__element block__element--one block__element--two');
  });

  it('should add modifiers as keys with non falsy nor empty values', () => {
    const modifiers = {
      'one': 'empty',
      'two': 123,
      'three four': true,
      'five': 0,
      'six': '',
      'seven': '  ',
      'eight': false,
      'nine': null,
      'ten': undefined,
    };

    expect(
      block({ modifiers }),
    ).toEqual('block block--one-empty block--two-123 block--three block--four block--five-0');
    expect(
      block({ element: 'element', modifiers }),
    ).toEqual('block__element block__element--one-empty block__element--two-123 block__element--three block__element--four block__element--five-0');
  });

  it('should add non falsy nor empty modifiers given in array', () => {
    const modifiers = [
      'one',
      'two three',
      123,
      '',
      0,
      null,
      false,
      undefined,
      { four: 'cats' },
    ];

    expect(
      block({ modifiers }),
    ).toEqual('block block--0 block--123 block--one block--two block--three block--four-cats');
    expect(
      block({ element: 'element', modifiers }),
    ).toEqual('block__element block__element--0 block__element--123 block__element--one block__element--two block__element--three block__element--four-cats');
  });

  it('should add extra given as string', () => {
    expect(
      block({ extra: 'extra' }),
    ).toEqual('block extra');
  });

  it('should split extra given as string', () => {
    expect(
      block({ extra: 'one   two' }),
    ).toEqual('block one two');
  });

  it('should add extra as keys with non falsy nor empty values', () => {
    const extra = {
      'one': 'empty',
      'two': 123,
      'three four': true,
      'five': 0,
      'six': '',
      'seven': '  ',
      'eight': false,
      'nine': null,
      'ten': undefined,
    };

    expect(
      block({ extra }),
    ).toEqual('block one two three four five');
  });

  it('should add non falsy nor empty extra given in array', () => {
    const extra = [
      'one',
      'two three',
      123,
      '',
      0,
      null,
      false,
      undefined,
      { four: 'cats' },
    ];

    expect(
      block({ extra }),
    ).toEqual('block 0 123 one two three four');
  });
});
